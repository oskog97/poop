'''
Forking version of multiprocessing.py for Anonymine on RustPython and
possibly some other platforms where multiprocessing module doesn't work.

It is NOT suitable as a multiprocessing substitute for any other project;
many functions and constants are completely missing.
'''

import os
import queue
import signal
import subprocess


def cpu_count():
    return os.cpu_count()


class Process():
    def __init__(self, target, args):
        self.target = target
        self.args = args

    def start(self):
        pid = os.fork()
        if pid:
            self.child_pid = pid
        else:
            self.target(*self.args)
            os._exit(0)

    def join(self):
        os.waitpid(self.child_pid, 0)

    def close(self):
        pass

    def terminate(self):
        '''Warning: Will send SIGTERM to the child's PID without any checks'''
        os.kill(self.child_pid, signal.SIGTERM)


def _alarm_handler(*whatever):
    raise queue.Empty


class Queue():
    '''
    Passes data through a pipe with repr and eval

    Simultaneous calls to put may cause data corruption

    Installs signal handler for SIGALRM
    '''

    def __init__(self):
        read_fd, write_fd = os.pipe()
        self.reader = os.fdopen(read_fd, "r")
        self.writer = os.fdopen(write_fd, "w")
        self.old_alarm_handler = signal.signal(signal.SIGALRM, _alarm_handler)

    def put(self, item):
        self.writer.write(repr(item) + '\x00')
        self.writer.flush()

    def get(self, block=True, timeout=None):
        if not block:
            raise NotImplementedError
        if timeout is not None:
            if timeout < 1: timeout = 1
            signal.alarm(int(timeout))
        # Get only the first byte with the alarm active
        packet = self.reader.read(1)
        signal.alarm(0)
        while packet[-1] != '\x00':
            packet += self.reader.read(1)
        return eval(packet[:-1])

    def __del__(self):
        signal.signal(signal.SIGALRM, self.old_alarm_handler)
        self.reader.close()
        self.writer.close()


def set_start_method(method):
    pass
