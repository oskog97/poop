'''
Minimal subprocess.py for Anonymine to work on platforms where the
subprocess module doesn't work.

It is NOT suitable as a subprocess substitute for any other project;
many functions and constants are completely missing.
'''

PIPE = None

def Popen(*args, **kwargs):
    '''Not implemented'''
    raise NotImplementedError
