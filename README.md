# Pööp

Really shitty replacements for Python standard modules to get Anonymine to
work on platforms where certain modules are missing, failing to import or
not working.

These are only meant for use with Anonymine.  Sometimes I do things
intentionally wrong because I know how the caller will handle the error.
Also: nearly all of the funtionality of the modules is completely missing.



## Installation
In the Anonymine directory:
```sh
git clone https://gitlab.com/oskog97/poop.git
ln -s poop/curses.py .              # if needed
ln -s poop/math.py .                # if needed
ln -s poop/multiprocessing.py .     # if needed
ln -s poop/multiprocessing-forking.py multiprocessing.py    # if needed
ln -s poop/subprocess.py .          # if needed
ln -s poop/threading.py .           # if needed
```
And then things should "work" in that directory.

### Permanent installation
Copy or symlink the files to your Python's site-packages.

*The word "usually" in the following paragraphs is meant to be interpreted
relatively.  Usually, none of this is usually missing, except curses.*

`multiprocessing`, `subprocess`, `threading` and `curses` are usually present
but dysfunctional and appear before site-packages in `sys.path`.  You need to
rename/move/remove the `subprocess.py*` and `threading.py*` files and/or the
`multiprocessing` and `curses` directories.
**Note**: Only
do this if the module actually is not working, one or both of them are likely
working, so check before you substitute them.

`math` is completely missing on Minix 3.3, so just installing in site-packages
is sufficient.

### Minix 3.3 or other ancient systems

**This hasn't been tested in a long time,** the latest tested version of
Anonymine is 0.6.9.

- Install `mozilla-rootcerts` and run `mozilla-rootcerts install` as root
- Use `wget https://gitlab.com/oskog97/poop/-/archive/main/poop-main.tar.gz`
  (The URL for Anonymine is
  `https://gitlab.com/oskog97/anonymine/-/archive/master/anonymine-master.tar`)
- Download the tarballs to a server on the local network and use an
  unencrypted connection.



## Modules

### `curses`

Missing on Jython, IronPython, GraalVM Python, RustPython, and on PyPy on
Windows.  (On CPython on Windows you can install `windows-curses` which
unsurprisingly is a LOT better.)

Implements close to the bare minimum required for Anonymine to work.

Will try to use `msvcrt.getch` or equivalent if available, otherwise defaults
to a very sucky cooked input mode where you can give multiple sequential inputs
followed by enter.

It spits out a whole lot of ANSI escape codes to stdout and assumes the
terminal will understand them.

Windows support:
- 11/Windows Terminal: Works out of the box
- 10/Windows Console Host: Requires registry setting
- older: Requires Ansicon.  If other VT100/ANSI compatibility software is
  used, you must set SHITE_CURSES_DUMBTERM=0

To work on Windows 10, you'll have to enable support for VT100+/ANSI escape
codes in conhost.  Run this in a admin Powershell:
```powershell
Set-ItemProperty HKCU:\Console VirtualTerminalLevel -Type DWORD 1
```
On older versions of Windows you can use
[Ansicon](https://github.com/adoxa/ansicon).
Ansicon supports detecting the window size, just run
```batch
echo %ANSICON%
```
to actually update the environment variable after you change the
window size.

You can set the ROWS and COLS environment variables to your desired size.
To avoid scrolling in Anonymine, set ROWS=height+3 and COLS=2*width+5.
With Ansicon you can change the console window size, but you need one
more column.

Example:
```batch
set ROWS=23
set COLS=45
pypy anonymine.py
```
Lower sizes run smoother.


### `math`

Missing on Minix 3.3.

Implements `math.ceil` and nothing else.  May contain alternative facts
about Minix 3.3.


### `multiprocessing`

Missing on Jython and Minix.

Broken on IronPython 2.7/3.4.

On RustPython you should use the forking version instead.

Implements pretty much nothing.


### `multiprocessing` (forking)

Symlink `multiprocessing-forking.py` as `multiprocessing.py` for RustPython
on unix-like OSes.

This version requires Python 3 and `os.fork` and `signal.alarm`.

The `multiprocessing` module can't be imported on RustPython (at the time
this was written) due to a lack of functioning `sem_open`.


### `subprocess`

Missing on Minix 3.3.

Implements nothing.


### `threading`

Missing on Minix 3.3 and 3.4.0rc6.

Implements `Timer` using `signal.alarm`.
