'''
Minimal multiprocessing.py for Anonymine to work on platforms where the
multiprocessing module doesn't work.

It is NOT suitable as a multiprocessing substitute for any other project;
many functions and constants are completely missing.
'''


try:
    import queue
except:
    import Queue as queue


def cpu_count():
    '''Not implemented'''
    raise NotImplementedError


class Process():
    '''Process.start will unconditionally fail'''

    def __init__(self, **kwargs):
        pass

    def start(self):
        # `Process.start` MUST fail for Anonymine to work as Anonymine
        # requires the child process to have a separate memory space.
        # Actually running the target will result in a bug.
        raise NotImplementedError

    def terminate(self):
        import sys
        sys.stderr.write('Process.terminate unexpectedly called\n')


class Queue(list):
    '''FIFO, not suitable for multiprocessing'''

    def put(self, item):
        self.append(item)

    def get(self, *ignore):
        if len(self) == 0:
            raise queue.Empty
        return self.pop(0)


def set_start_method(method):
    raise ValueError
