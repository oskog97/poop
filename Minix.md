# Installing Anonymine on Minix

**Note**: It works better on Minix 3.4

Only a replacement for `multiprocessing` and `threading` is needed on
Minix 3.4.0rc6 (the latest snapshot).  Minix 3.3 needs a replacement for
`math` and `subprocess` too.

`import multiprocessing` fails because it can't import `thread`.
site-packages comes after the standard modules, so you can't just override
the broken multiprocessing with another one, you have to either rename/remove
the standard module or have the "fixed" `multiprocessing.py` in the current
directory when running Anonymine.

Similar thing for `subprocess` on Minix 3.3.  The `math` module, however,
doesn't exist at all.

If you intend to install: It would probably be less steps to immediately
install modules from Pööp first (and hide the broken standard modules),
instead of first creating symlinks in the local directory.

It is possible to run Anonymine from the tarball directory, but you should
probably tweak `enginecfg.out` first and create the highscores file.
Run with `python2.7 anonymine -c cursescfg -e enginecfg.out`.

## Minix 3.4.0rc6

After having installed Minix on a VM, log in as root:
```shell
pkgin update
pkgin install python27 py27-curses git  # REQUIRED
# Not required:
pkgin install nano wget lynx            # nice utilities
nano /usr/pkg/etc/nanorc                #
pkgin install py27-readline             # Makes interactive Python nicer
#pkgin install mozilla-rootcerts        # python27 depends on this anyway
# Required for git and anything else that uses https:
mozilla-rootcerts install
# Add a user:
useradd -G wheel -m user
passwd user
echo "umask 022" >>/home/user/.shrc
```

Login as user:
```shell
git clone https://gitlab.com/oskog97/anonymine.git
cd anonymine
git clone https://gitlab.com/oskog97/poop.git
ln -s poop/multiprocessing.py .
ln -s poop/threading.py .
./configure -v
make
su
```
root:
```shell
make install

# 'import multiprocessing' fails anyway so lets just replace it,
# same with 'threading'.
mkdir /usr/pkg/lib/python2.7/broken
mv /usr/pkg/lib/python2.7/multiprocessing/ /usr/pkg/lib/python2.7/broken
mv /usr/pkg/lib/python2.7/threading /usr/pkg/lib/python2.7/broken
cp poop/multiprocessing.py /usr/pkg/lib/python2.7/site-packages/
cp poop/threading.py /usr/pkg/lib/python2.7/site-packages/

exit
```

`anonymine` can now be run from anywhere.


## Minix 3.3

TL;DR Just don't.

**This hasn't been tested in a long time,** the latest tested version of
Anonymine is 0.6.9.

After having installed Minix on a VM, log in as root:
```shell
pkgin update
pkgin install python27 py27-curses wget # REQUIRED
# Not required:
pkgin install nano lynx                 # nice utilities
nano /usr/pkg/etc/nanorc                #
pkgin install py27-readline             # Makes interactive Python nicer
# Required for wget and anything else that uses https:
# Doesn't work
#mkdir -p /usr/pkg/etc/openssl/certs
#pkgin install mozilla-rootcerts
#mozilla-rootcerts install
# Add a user:
useradd -m user
passwd user
```

Login as user:
```shell
#wget --no-check-certificate https://oskog97.com/archive/anonymine/anonymine-0.6.1.tar.xz
wget --no-check-certificate https://gitlab.com/oskog97/anonymine/-/archive/master/anonymine-master.tar.bz2
wget --no-check-certificate https://gitlab.com/oskog97/poop/-/archive/main/poop-main.tar.bz2
bunzip2 *.bz2
tar -xf anonymine-master.tar
tar -xf poop-main.tar
cd anonymine-master
ln -s ../poop-main/math.py .
ln -s ../poop-main/multiprocessing.py .
ln -s ../poop-main/subprocess.py .
ln -s ../poop-main/threading.py .
./configure -v
make
su
```
root:
```shell
make install

# There is no 'math' module at all
# 'import multiprocessing' fails anyway so lets just replace it
# 'import subprocess' fails anyway so lets just replace it
mkdir /usr/pkg/lib/python2.7/broken
mv /usr/pkg/lib/python2.7/multiprocessing/ /usr/pkg/lib/python2.7/broken
mv /usr/pkg/lib/python2.7/subprocess.py* /usr/pkg/lib/python2.7/broken
mv /usr/pkg/lib/python2.7/threading.py* /usr/pkg/lib/python2.7/broken
cp ../poop-main/math.py /usr/pkg/lib/python2.7/site-packages/
cp ../poop-main/multiprocessing.py /usr/pkg/lib/python2.7/site-packages/
cp ../poop-main/subprocess.py /usr/pkg/lib/python2.7/site-packages/
cp ../poop-main/threading.py /usr/pkg/lib/python2.7/site-packages/

exit
```

`anonyine` can now be run from anywhere.
