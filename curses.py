# encoding: utf-8

# Copyright (c) 2022-2024, Oskar Skog
# Released under the 2-clause BSD license.

r'''
Minimal curses.py for Anonymine to work on platforms without curses

It is NOT suitable as a curses substitute for any other project;
many functions and constants are completely missing.  Using functions
incorrectly (or just in ways not supported by this replacement)  will
result in exceptions not raised by the real curses library.
The doc strings in this module don't explain how to use curses, they
explain how things here are different.


Input mode:
    - "getch"
        - Windows CPython/PyPy: msvcrt.getch()
            - Supports arrow keys
        - IronPython: clrtype.System.Console.ReadKey().KeyChar
        - Jython: readline._console.reader.readCharacter()
            - Supports arrow keys on VT100 compatible terminal
            - On Windows: Use `jython.exe -i`, the `-i` flag is needed
        - Any system with stty(1) command
    - Cooked input mode
        - Used if nothing else exists or if their getch implementation fails
        - Affected by environment variables (see below)
        - Simulates 80x23 terminal with the 24th line for
          line-buffered input.
        - Please refer to the disclaimer.

Output mode:
    - Modes
        - ANSI escape codes (VT100 + color)
        - Dumb terminal, please refer to the disclaimer.
    - Automatic size detection on IronPython, semi-automatic with Ansicon


VT100 output mode on Windows:
    If you're using CPython, use windows-curses instead.
        pip install windows-curses

    Works out of the box on Windows 11.  (Windows Terminal)

    Windows 10 supports ANSI escape codes in the console, but it is
    disabled by default.  To enable, run this in an
    Administrator Powershell:
        Set-ItemProperty HKCU:\Console VirtualTerminalLevel -Type DWORD 1

    On older versions of Windows, you can use Ansicon:
    https://github.com/adoxa/ansicon
    Ansicon supports detecting the window size, just run
        echo %ANSICON%
    to actually update the environment variable after you change the
    window size.

    If you use an older version of Windows and aren't using Ansicon, no
    ANSI escape codes will be emitted.  Please refer to the disclaimer.


Environment variables:
    SHITE_CURSES_FORCE_COOKED   Integer, default 0 if not set.
        Set to 1 to force cooked mode input.

    SHITE_CURSES_GETCH_DELAY    Floating point, default 0.1 if not set.
        Delay between buffered inputs in seconds when in cooked mode.

    SHITE_CURSES_DUMBTERM       Integer.  Set to 1 to disable VT100
        escape sequences.  Default 0 on most platforms, default 1 on
        old Windows or if TERM=dumb.

    SHITE_CURSES_NOCOLOR        Integer.  Set to 1 to disable color.
        May speed up Windows console and hopefully make IronPython be
        slightly less shit on Windows.

    ROWS        Integer, default 24.  (Internally one less when in
                cooked input mode.)  Do not use this if you use ansicon.

    COLS        Integer default, 80  Do not use this if you use ansicon.

    ANSICON     Set by ansicon if you use that. Contains console window size.

    TERM        Only a few special values are recognised:
                    'dumb'              Same as SHITE_CURSES_DUMBTERM=1
                    vt*                 Same as SHITE_CURSES_NOCOLOR=1
                    'linux'             Mostly normal

    Invalid values will cause ValueError exception to be raised on
    import.


Disclaimer:
    IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY
    DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
    (INCLUDING, BUT NOT LIMITED TO, COMPLETE LOSS OF SANITY, EYESTRAIN,
    MIGRAINES, CONSTANT SWEARING, PARTIAL LOSS OF SANITY, OR COMPLETE LOSS OF
    SANITY) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
    STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
    ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
    OF SUCH DAMAGE.
'''

import os
import platform
import sys
import time

version = 'Pööp'


### Input mode
# The input_mode variable gets set by the first successful method and
# is assigned a unique value for debugging purposes.  If cooked_input is
# False, there will be a `getch` function defined which returns ASCII values
# or for arrow keys either the same thing as raw input from a VT100 or
# msvcrt.getch().  'stty' needs special handling in various places
input_mode = None

# Force cooked
if input_mode is None:
    if os.getenv('SHITE_CURSES_FORCE_COOKED') == '1':
        cooked_input = True
        input_mode = 'force-cooked'

# CPython/PyPy/IronPython on Windows
if input_mode is None:
    try:
        from msvcrt import getch
        cooked_input = False
        input_mode = 'msvcrt.getch()'
    except ImportError:
        pass

# IronPython on any platform
if input_mode is None:
    try:
        import clrtype
        def getch():
            sys.stdout.write('\r')
            sys.stdout.flush()
            return clrtype.System.Console.ReadKey().KeyChar
        cooked_input = False
        input_mode = 'clrtype.System.Console.ReadKey().KeyChar'
    except ImportError:
        pass

# Jython on any platform
if input_mode is None:
    try:
        # Need to use `jython -i` on Windows to import readline
        import readline
        assert '_console' in dir(readline)
        def getch():
            return chr(readline._console.reader.readCharacter())
        cooked_input = False
        input_mode = 'readline._console.reader.readCharacter()'
    except:
        pass

# Use stty(1), for GraalVM, but "should" be portable
# Doesn't help with IronPython (on Mono) on Linux
# Incompatible with Jython on Linux worse
if input_mode is None:
    if os.system("stty >/dev/null") == 0:
        def getch():
            return sys.stdin.read(1)
        cooked_input = False
        input_mode = "stty"

# Only cooked mode is supported
if input_mode is None:
    cooked_input = True
    input_mode = 'cooked'

cooked_input_delay = float(os.getenv('SHITE_CURSES_GETCH_DELAY', '0.1'))


### Output mode

# Some terminals will start on the next line immediately when all columns
# are used up, and the newline character would cause blank lines, so the
# variable dumb_term will later be used in `COLS -= dumb_term`
default_dumb_term = False

# Disable VT100 on old Windows except if you have ansicon
if os.name == 'nt':
    try:
        ansicon = os.getenv('ANSICON')
        size = ansicon.split('(')[1].split(')')[0]
        test_COLS, test_ROWS = map(int, size.split('x'))
    except:
        if int(platform.version().split('.')[0]) < 10:
            default_dumb_term = True

# Actual DUMB terminal
if os.getenv('TERM', '') == 'dumb':
    if platform.python_implementation() != 'IronPython':
        default_dumb_term = True

# Explicitly set environment variable
dumb_term = int(os.getenv('SHITE_CURSES_DUMBTERM', default_dumb_term))


### Disable color option
default_disable_color = False

if os.getenv('TERM', '').startswith('vt'):
    default_disable_color = True

if os.name == 'nt' and platform.python_implementation() == 'IronPython':
    default_disable_color = True

disable_color = int(os.getenv('SHITE_CURSES_NOCOLOR', default_disable_color))


# Linux console doesn't support dim+reverse
linux_term = os.getenv('TERM') == 'linux'


### Terminal size

# Default terminal size:
COLS, ROWS = 80, 24

# Ansicon:
if os.getenv('ANSICON') is not None:
    ansicon = os.getenv('ANSICON')
    size = ansicon.split('(')[1].split(')')[0]
    COLS, ROWS = map(int, size.split('x'))
    COLS -= 1   # https://github.com/adoxa/ansicon/issues/137

# IronPython:
try:
    import clrtype
    ROWS = clrtype.System.Console.WindowHeight
    COLS = clrtype.System.Console.WindowWidth - bool(os.getenv('ANSICON'))
except:
    pass

# Override:
ROWS = int(os.getenv('ROWS', ROWS))
COLS = int(os.getenv('COLS', COLS))
# One line at the bottom for cooked mode input:
ROWS -= cooked_input
# Some terminals will start on the next line immediately when all columns
# are used up, and the newline character would cause blank lines:
COLS -= dumb_term



### Constants

# Special return values MUST be integers.  Using string will mess things up.
# Example:
# anonymine.py (curses_game.input) line 769:
#     self.cheatcode_input = self.cheatcode_input[-64:] + mychr(ch%256)
# TypeError: not all arguments converted during string formatting
class SpecialKey(int):
    message = ''
    def __repr__(self):
        return self.message
    def __str__(self):
        return self.message
def new_SpecialKey(message):
    value = hash(message) & 0x7fffff00 | 0x40000000
    retval = SpecialKey(value)
    retval.message = message
    return retval

# ERR is never returned, but caller will check for it
# MUST be different from no-op keys
ERR                 = new_SpecialKey('curses.ERR')
KEY_UNKNOWN         = new_SpecialKey('curses.KEY_UNKNOWN')
KEY_MOUSE           = new_SpecialKey('curses.KEY_MOUSE')

                                # msvcrt.getch()    VT100
KEY_UP              = 259       # \xe0H             \x1b[A
KEY_LEFT            = 260       # \xe0K             \x1b[D
KEY_DOWN            = 258       # \xe0P             \x1b[B
KEY_RIGHT           = 261       # \xe0M             \x1b[C
arrow_keys = {
                                72: 259,            65: 259,
                                75: 260,            68: 260,
                                80: 258,            66: 258,
                                77: 261,            67: 261,
}

# Attributes (bitmask):
A_NORMAL            = 0         # Value 0 required for no-op attribute
A_REVERSE           = 1<<18
A_BLINK             = 1<<19     # 1<<6 minimum as color is encoded in the
A_DIM               = 1<<20     # low 6 bits.  These values are copied from
A_BOLD              = 1<<21     # real curses.
A_INVIS             = 1<<23
A_STANDOUT          = 0         # Used by Anonymine to detect NetBSD and
                                # anything else with the same bug.

if disable_color:
    A_INVIS = 0

# Output ("\x1b[%dm" % attrib_codes[attribute])
attrib_codes = {
    A_REVERSE:      7,
    A_BLINK:        5,
    A_DIM:          2,
    A_BOLD:         1,
    A_INVIS:        8,
}

# Colors:           # BGR   (Order used by standard)
COLOR_BLACK         = 0
COLOR_RED           = 1
COLOR_GREEN         = 2
COLOR_YELLOW        = 3
COLOR_BLUE          = 4
COLOR_MAGENTA       = 5
COLOR_CYAN          = 6
COLOR_WHITE         = 7

def noop(*args):
    '''Doesn't do anything'''
    pass

cbreak              = noop
def_prog_mode       = noop
def_shell_mode      = noop      # Not used by Anonymine
echo                = noop
meta                = noop
nocbreak            = noop
noecho              = noop
reset_prog_mode     = noop
reset_shell_mode    = noop
start_color         = noop

# Anonymine expects mouse related functions to not exist at all, ie raise
# AttributeError if referenced.


if input_mode == "stty":
    def cbreak():   os.system("stty cbreak || stty -icanon min 1 time 0")
    def noecho():   os.system("stty -echo")
    def nocbreak(): os.system("stty cooked")
    def echo():     os.system("stty echo")


def endwin():
    '''Reset attributes, jump to upper left corner and clear screen'''
    if input_mode == "stty":
        os.system("stty cooked echo")
    if dumb_term:
        sys.stdout.write('\n\n\n\n\n\n')
    else:
        sys.stdout.write('\x1b[0m\x1b[1;1H\x1b[J')
    sys.stdout.flush()


def has_colors():
    '''Always True'''
    return True

color_pairs = [(COLOR_WHITE, COLOR_BLACK)]

def init_pair(index, foreground, background):
    # Ensure the index is valid
    while index >= len(color_pairs):
        color_pairs.append((COLOR_WHITE, COLOR_BLACK))
    color_pairs[index] = (foreground, background)

def color_pair(index):
    '''
    The lower six bits of attributes contain foreground and background
    color.  (foreground<<3 | background)

    attributes = color_pair(color_pair_index) | other_attributes
    '''
    if disable_color:
        index = 0
    return color_pairs[index][0]<<3 | color_pairs[index][1]


class initscr():
    def __init__(self):
        self.bkgdset(ord(' '), COLOR_WHITE<<3 | COLOR_BLACK)
        self.erase()
        self.input_buffer = []

    def keypad(self, bool):
        '''no-op'''
        pass
    def getmaxyx(self):
        return (ROWS, COLS)
    def move(self, foo, bar):
        '''no-op'''
        pass

    def bkgdset(self, char, attributes):
        self.background = (chr(char), attributes)
    def erase(self):
        self.screen = [
            [self.background for x in range(COLS)] for y in range(ROWS)
        ]

    def addstr(self, y, x, str, attributes):
        for index, char in enumerate(str):
            self.screen[y][x+index] = (char, attributes)

    # Output methods:
    def _refresh_dumb(self):
        parts = ['\n\n\n']
        for y in range(ROWS):
            for x in range(COLS):
                char, attributes = self.screen[y][x]
                parts.append(char)
            parts.append('\n')
        del parts[-1]       # Delete trailing newline
        return parts

    def _refresh(self):
        parts = ['\x1b[1;1H']       # Cursor to row 1, column 1
        for y in range(ROWS):
            # Avoid some repetition
            fragments = []   # (attributes, ['a', 'b', 'c']), ...
            last_attributes = None
            for x in range(COLS):
                char, attributes = self.screen[y][x]
                if attributes != last_attributes:
                    fragments.append((attributes, []))
                fragments[-1][1].append(char)
                last_attributes = attributes
            # Output
            for attributes, string in fragments:
                # Set attributes
                #   attrib = 1<<n
                #   attrib_codes = {attrib: SGR_number}
                #   Esc '[' %d 'm'
                if linux_term:
                    # Linux console doesn't support dim+reverse
                    # Let reverse take priority over dim
                    if attributes & A_DIM and attributes & A_REVERSE:
                        attributes -= A_DIM
                for attrib in attrib_codes:
                    if attributes & attrib:
                        parts.append('\x1b[%dm' % attrib_codes[attrib])
                # Set foreground color: Esc '[' (30-37) 'm'
                # Set background color: Esc '[' (40-47) 'm'
                if not disable_color:
                    parts.append('\x1b[%dm\x1b[%dm' % (
                        30 + (attributes>>3)%8,     # foreground color
                        40 + attributes%8           # background color
                    ))
                # Print characters
                # Reset attributes: Esc '[' '0' 'm'
                parts.append(''.join(string) + '\x1b[0m')
            parts.append('\n')
        del parts[-1]       # Delete trailing newline
        return parts

    def refresh(self):
        if dumb_term:
            parts = self._refresh_dumb()
        else:
            parts = self._refresh()
        # Prompt for input
        global cooked_input
        if cooked_input:
            parts.append('\nCooked input: %s' % ''.join(self.input_buffer))
            if not dumb_term:
                # Esc '[' 'J' clears the rest of the screen
                parts.append('\x1b[J')
        sys.stdout.write(''.join(parts))
        sys.stdout.flush()

    def redrawwin(self):
        # '\x1b[1;1H'  Cursor to row 1, column 1
        # Esc '[' 'J'  clears the rest of the screen
        if not dumb_term:
            sys.stdout.write('\x1b[1;1H\x1b[J')
        self.refresh()


    def getch(self):
        '''
        Implements raw/cbreak mode if possible. Not sure if ctrl+C generates
        KeyboardInterrupt or 0x03 with `getch`.

        If raw/cbreak mode is not possible it will be crudely emulated.
        '''
        global cooked_input
        if not cooked_input:
            try:
                char = ord(getch())
                # Is it maybe an arrow key
                if char == 0xe0 or char == 0x00:
                    # msvcrt.getch() returns 0x00 or 0xe0 for special keys
                    # Arrows keys are all 0xe0 * in the command window
                    # but Cygwin's terminal uses 0x00
                    pass
                elif char == 0x1b:
                    # VT100, Jython
                    # Eat the character after the escape
                    if getch() != '[':
                        return KEY_UNKNOWN
                else:
                    # Normal key
                    return char
                # May be an arrow key
                char = ord(getch())
                try:
                    return arrow_keys[char]
                except KeyError:
                    return KEY_UNKNOWN
            except KeyboardInterrupt:
                raise
            except Exception as err:
                # 2024-12-01, IronPython 2.7/3.4, Windows 11
                # SystemError DLL msvcr100 can't be loaded, 0x8007007e
                # Needs Microsoft Visual C++ 2010 Redistributable
                if isinstance(err, SystemError):
                    print()
                    print(err)
                    print('Press enter to acknowledge the error')
                    sys.stdin.readline()
                # Can fail if not on a real console
                global ROWS
                ROWS -= 1
                cooked_input = True
        # Cooked input
        # do {} while (!input_buffer);
        while True:
            while not self.input_buffer:
                sys.stdout.write(
                    '\x1b[%d;%dH'
                    '\x1b[J'
                    'Cooked input: '
                    % (ROWS+1, 1)
                )
                sys.stdout.flush()
                self.input_buffer = list(sys.stdin.readline().rstrip('\n'))
            # Escapes:
            if self.input_buffer[0] == '\\' and len(self.input_buffer) >= 2:
                self.input_buffer.pop(0)
                inchar = self.input_buffer[0]
                if inchar  == 'n':
                    outchar = '\n'
                elif inchar == 't':
                    outchar = '\t'
                else:
                    outchar = inchar
                self.input_buffer[0] = outchar
            # while (!input_buffer);
            if self.input_buffer:
                break
        # Delay for aesthetic reasons.  Staged inputs will animate better.
        time.sleep(cooked_input_delay)
        # Check for VT100 arrow key
        if self.input_buffer[:2] == ['\x1b', '[']:
            if len(self.input_buffer) >= 3:
                char = self.input_buffer[2]
                del self.input_buffer[:3]
                try:
                    return arrow_keys[ord(char)]
                except KeyError:
                    return ord(char)
        return ord(self.input_buffer.pop(0))
