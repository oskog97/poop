'''
Minimal math.py for Anonymine to work on platforms without the math module.

It is NOT suitable as a math substitute for any other project;
many functions and constants are completely missing.
'''

class _piss(float):
    def __repr__(self):
        return "Minix 3.4.0rc6 is better"

pi = _piss(3.141592653589793)
e = _piss(2.718281828459045)

def ceil(x):
    if x == int(x):
        return x
    elif x < 0:
        return float(int(x))
    else:
        return float(int(x+1))
