'''
Minimal threading.py for Anonymine to work on platforms where the
threading module doesn't work.

It is NOT suitable as a subprocess substitute for any other project;
many functions and constants are completely missing.
'''

try:
    import math
    import signal
except:
    pass

class Timer():
    def __init__(self, time, callback):
        self.time = time
        try:
            def alarm(*args):
                callback()
            signal.signal(signal.SIGALRM, alarm)
        except:
            pass

    def start(self):
        try:
            signal.alarm(int(math.ceil(self.time)))
        except:
            pass

    def cancel(self):
        try:
            signal.alarm(0)
        except:
            pass


def active_count():
    '''
    Always returns 2 to satisfy an
        assert threading.active_count() > 1
    assertion after starting a timer.
    '''
    return 2
